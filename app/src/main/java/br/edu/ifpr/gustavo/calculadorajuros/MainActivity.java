package br.edu.ifpr.gustavo.calculadorajuros;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener{
    private TextView txtgorgeta;
    private EditText txtValor;
    private TextView txtotal;
    private SeekBar porcentagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtValor = findViewById(R.id.txtPreco);
        txtgorgeta = findViewById(R.id.txtGorgeta);
        txtotal = findViewById(R.id.txtTotal);
        porcentagem = findViewById(R.id.taxas);

        porcentagem.setOnSeekBarChangeListener(this);

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        double resultado = Double.valueOf(txtValor.getText().toString()) * progress / 100;
        txtgorgeta.setText(String.format("%.2f",resultado)+"");
        txtotal.setText(String.format("%.2f", Double.valueOf(txtValor.getText().toString()) + resultado)+"");
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
